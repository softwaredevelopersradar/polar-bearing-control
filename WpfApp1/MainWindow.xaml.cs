﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Random random = new Random();
        private double[] GenerateDataX()
        {
            int iCount = 360;
            double[] points = new double[iCount];
            for (int i = 0; i < iCount; i++)
            {
                points[i] = random.NextDouble() * 3000;
            }
            return points;
        }

        private double[] GenerateDataY()
        {
            int iCount = 360;
            double[] points = new double[iCount];
            for (int i = 0; i < iCount; i++)
            {
                points[i] = random.NextDouble() * 360;
            }
            return points;
        }

        private void one_Click(object sender, RoutedEventArgs e)
        {
            polarBearing.BearingPainted(GenerateDataX(), GenerateDataY());
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            polarBearing.ClearBearing();
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            polarBearing.AxisColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            polarBearing.LabelsColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            polarBearing.BackGround = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void six_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            polarBearing.PointColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            polarBearing.MarkerTextColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void eight_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            polarBearing.GraphBackGround = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void nine_Click(object sender, RoutedEventArgs e)
        {
            polarBearing.SetLanguage("rus");
        }

        private void ten_Click(object sender, RoutedEventArgs e)
        {
            polarBearing.SetLanguage("eng");
        }

        private void eleven_Click(object sender, RoutedEventArgs e)
        {
            polarBearing.SetLanguage("az");
        }
    }
}
