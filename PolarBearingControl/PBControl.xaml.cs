﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.EventMarkers;

namespace PolarBearingControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class PBControl : UserControl
    {
        #region Conctructors

        public PBControl()
        {
            InitializeComponent();
            InitStartParams();

            //GenerateData();
        }

        private void GenerateData()
        {
            int pointCount = 100;
            PolarSeriesPoint[] dataPoints = new PolarSeriesPoint[pointCount];
            Random random = new Random();
            for (int i = 0; i < pointCount; i++)
            {
                dataPoints[i].Angle = random.Next(0, 360);
                dataPoints[i].Amplitude = random.Next(26, 90);
            }

            PLSP.Points = dataPoints;
        }

        private void InitStartParams()
        {
            _GlobalNumberOfBands = 100;
            _GlobalBandWidthMHz = 30.0;
            _GlobalRangeMin = 25;
            _GlobalRangeMax = _GlobalRangeMin + _GlobalNumberOfBands * _GlobalBandWidthMHz;
        }
        

        #endregion

        #region Properties

        private double _GlobalRangeMin;
        private double GlobalRangeMin
        {
            get { return _GlobalRangeMin; }
            set
            {
                _GlobalRangeMin = value;
                _ReCalc();
            }
        }

        private double _GlobalRangeMax = 0;
        private double GlobalRangeMax
        {
            get { return _GlobalRangeMax; }
            set { _GlobalRangeMax = value; }
        }

        private int _GlobalNumberOfBands;
        private int GlobalNumberOfBands
        {
            get { return _GlobalNumberOfBands; }
            set
            {
                _GlobalNumberOfBands = value;
                _ReCalc();
            }
        }

        private double _GlobalBandWidthMHz;
        private double GlobalBandWidthMHz
        {
            get { return _GlobalBandWidthMHz; }
            set
            {
                _GlobalBandWidthMHz = value;
                _ReCalc();
            }
        }

        public double MaxAmplitude
        {
            get { return polarAxis.MaxAmplitude; }
            set { polarAxis.MaxAmplitude = value;}
        }

        private int _LiveTime = 3000;
        public int LiveTime
        {
            get { return _LiveTime; }
            set { _LiveTime = value; }
        }


        private Color _LabelsColor;
        public Color LabelsColor
        {
            get { return _LabelsColor; }
            set
            {
                _LabelsColor = value;
                polarAxis.MajorDivTickStyle.Color = value;
                polarAxis.GridAngular.LabelsColor = value;
            }
        }

        private Color _AxisColor;
        public Color AxisColor
        {
            get { return _AxisColor; }
            set
            {
                _AxisColor = value;
                polarAxis.AxisColor = value;
            }
        }

        private Color _BackGround;
        public Color BackGround
        {
            get { return _BackGround; }
            set
            {
                _BackGround = value;

                polarBearingChart.ChartBackground.Color = value;
                //chart.ChartBackground.GradientFill = GradientFill.Solid;
            }
        }

        private Color _GraphBackGround;
        public Color GraphBackGround
        {
            get { return _GraphBackGround; }
            set
            {
                _GraphBackGround = value;
                polarBearingChart.ViewPolar.GraphBackground.Color = value;
            }
        }

        private Color _PointColor;
        public Color PointColor
        {
            get { return _PointColor; }
            set
            {
                _PointColor = value;
                pointStyle.Color1 = Color.FromArgb(50, value.R, value.G, value.B);
            }
        }

        private Color _MarkerTextColor = Colors.White;
        public Color MarkerTextColor
        {
            get { return _MarkerTextColor; }
            set
            {
                _MarkerTextColor = value;

                for (int i = 0; i < polarBearingChart.ViewPolar.Markers.Count; i++)
                {
                    polarBearingChart.ViewPolar.Markers[i].Label.Color = value;
                }
            }
        }


        #endregion

        #region Public Metods

        private void PlotData(double[] dataArrayX, double[] dataArrayY)
        {
            int pointCounter = dataArrayX.Length;

            var points = new PolarSeriesPoint[pointCounter];

            for (int i = 0; i < pointCounter; i++)
            {
                points[i].Amplitude = dataArrayX[i];
                points[i].Angle = dataArrayY[i];
            }

            PLSP.Points = points;
        }

        private struct BearingPoint
        {
            public double[] xdata;
            public double[] ydata;
            public DateTime datetime;

            public BearingPoint(double[] xData, double[] yData, DateTime dateTime)
            {
                xdata = xData;
                ydata = yData;
                datetime = dateTime;
            }
        };
        List<BearingPoint> BearingPoints = new List<BearingPoint>();
        public void BearingPainted(double[] xData, double[] yData)
        {
            PLSP.Points = new PolarSeriesPoint[0];

            byte Intens = 50;

            pointStyle.Color1 = Color.FromArgb(Intens, pointStyle.Color1.R, pointStyle.Color1.G, pointStyle.Color1.B);

            BearingPoints.Add(new BearingPoint(xData, yData, DateTime.Now));

            for (int i = 0; i < BearingPoints.Count - 1; i++)
            {
                DateTime now = DateTime.Now;
                var diff = now - BearingPoints[i].datetime;
                if (diff.TotalMilliseconds > _LiveTime)
                {
                    BearingPoints.RemoveAt(i);
                }
            }

            double[] plotXData = new double[0];
            double[] plotYData = new double[0];

            for (int i = 0; i < BearingPoints.Count; i++)
            {
                var tempArray = new double[plotXData.Length + BearingPoints[i].xdata.Length];
                plotXData.CopyTo(tempArray, 0);
                BearingPoints[i].xdata.CopyTo(tempArray, plotXData.Length);
                plotXData = tempArray;

                tempArray = new double[plotYData.Length + BearingPoints[i].ydata.Length];
                plotYData.CopyTo(tempArray, 0);
                BearingPoints[i].ydata.CopyTo(tempArray, plotYData.Length);
                plotYData = tempArray;
            }
            PlotData(plotXData, plotYData);
        }

        public void ClearBearing()
        {
            PLSP.Points = new PolarSeriesPoint[0];
            BearingPoints.Clear();
        }

        #endregion

        public void SetLanguage(string param)
        {
            if (param.ToLower().Contains("ru"))
            {
                _MHz = "МГц";
                TranslateMarkers(_MHz);
            }
            if (param.ToLower().Contains("en"))
            {
                _MHz = "MHz";
                TranslateMarkers(_MHz);
            }
            if (param.ToLower().Contains("az"))
            {
                _MHz = "MHs";
                TranslateMarkers(_MHz);
            }
        }

        private void TranslateMarkers(string text)
        {
            for (int i = 0; i < polarBearingChart.ViewPolar.Markers.Count(); i++)
            {
                //var str = polarBearingChart.ViewPolar.Markers[i].Label.Text.Replace("МГц", "MHz");
                //polarBearingChart.ViewPolar.Markers[i].Label.Text = str;
                polarBearingChart.ViewPolar.Markers[i].Label.Text = _MHz;
            }
        }

        string _MHz = "МГц";

        #region Special Metods

        //Add marker for each point, to act as an editing point
        private void AddMarkers(PolarSeriesPoint[] points)
        {
            //FFPLS.SeriesEventMarkers.Clear();
            for (int i = 0; i < points.Count(); i++)
            {
                PolarEventMarker marker = new PolarEventMarker();
                marker.Amplitude = points[i].Amplitude;
                marker.AngleValue = points[i].Angle;
                //store values in label text 
                marker.Label.Text = points[i].Amplitude.ToString("0.0000") + " " + _MHz +  "\r\n" + points[i].Angle.ToString("0.0") + "°";
                marker.Label.HorizontalAlign = AlignmentHorizontal.Center;
                //marker.Label.Font = new WpfFont("Segoe UI", 14, true, false);
                //marker.Label.Font = new WpfFont("Segoe UI", 12, false, false);
                marker.Label.Shadow.Style = TextShadowStyle.HighContrast;
                // marker.Label.Shadow.ContrastColor = Colors.Black;
                marker.Label.Shadow.ContrastColor = Colors.Transparent;
                marker.Label.VerticalAlign = AlignmentVertical.Top;
                marker.Label.Visible = true;
                marker.Label.Color = _MarkerTextColor;
                marker.Symbol.GradientFill = GradientFillPoint.Solid;
                //marker.Symbol.Color1 = Colors.Orange;
                marker.Symbol.Color1 = Colors.Transparent;
                marker.Symbol.Width = 5;
                marker.Symbol.Height = 5;
                marker.Symbol.Shape = Arction.Wpf.SemibindableCharting.Shape.Circle;
                marker.MoveByMouse = false;
                marker.MouseOverOn += new MouseEventHandler(marker_MouseOverOn);
                marker.MouseOverOff += new MouseEventHandler(marker_MouseOverOff);
                marker.Label.Visible = false;

                if (!polarBearingChart.ViewPolar.Markers.Contains(marker))
                    polarBearingChart.ViewPolar.Markers.Add(marker);
            }
        }

        private void marker_MouseOverOn(object sender, MouseEventArgs e)
        {
            ((PolarEventMarker)sender).Label.Visible = true;
        }

        private void marker_MouseOverOff(object sender, MouseEventArgs e)
        {
            ((PolarEventMarker)sender).Label.Visible = false;
            polarBearingChart.ViewPolar.Markers.Remove(((PolarEventMarker)sender));
        }

        #endregion

        #region Private Metods

        private void SetOriginScale()
        {
            SetXRange(_GlobalRangeMin,_GlobalRangeMax);
        }

        private void Clear()
        {
            PLSP.Points = new PolarSeriesPoint[0];
        }

        private void _ReCalc()
        {
            _GlobalRangeMax = _GlobalRangeMin + _GlobalNumberOfBands * _GlobalBandWidthMHz;
            SetXRange(_GlobalRangeMin, _GlobalRangeMax);
        }

        private void SetXRange(double start, double end)
        {
            polarBearingChart.ViewPolar.Axes[0].SetRange(start, end);
        }

        #endregion


        //private void polarBearingChart_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    Point mousePos = e.GetPosition(polarBearingChart);
        //    double amplitude = GetMouseAmplitude(mousePos);
        //    double angle = GetMouseAngle(mousePos);

        //    var point = e.GetPosition(polarBearingChart);
        //    if (polarBearingChart.ViewPolar.PointLineSeries[0].SolveNearestDataPointByCoord(point.X, point.Y, out var nearestAngleValue, out var nearestAmplitudeValue))
        //    {
        //    }
        //}
        private double GetMouseAngle(Point mouseCoord)
        {
            double angle;
            double amplitude;
            polarBearingChart.ViewPolar.Axes[0].CoordToValue((int)Math.Round(mouseCoord.X), (int)Math.Round(mouseCoord.Y), out angle, out amplitude, true);

            return angle;
        }
        private double GetMouseAmplitude(Point mouseCoord)
        {
            double angle;
            double amplitude;
            polarBearingChart.ViewPolar.Axes[0].CoordToValue((int)Math.Round(mouseCoord.X), (int)Math.Round(mouseCoord.Y), out angle, out amplitude, true);

            return amplitude;
        }


        #region ChartHandlers
        private void polarBearingChart_MouseMove(object sender, MouseEventArgs e)
        {
            if (PLSP != null)
            {
                if (PLSP.Points != null)
                {
                    var point = e.GetPosition(polarBearingChart);
                    if (polarBearingChart.ViewPolar.PointLineSeries[0].SolveNearestDataPointByCoord(point.X, point.Y, out var nearestAngleValue, out var nearestAmplitudeValue))
                    {
                        //double amplitude = GetMouseAmplitude(new Point(xValue,yValue));
                        //double angle = GetMouseAngle(new Point(xValue, yValue));
                        PolarSeriesPoint pSP = new PolarSeriesPoint(nearestAngleValue, nearestAmplitudeValue);
                        AddMarkers(new PolarSeriesPoint[] { pSP });
                    }
                }
            }
        }
        #endregion
    }
}
